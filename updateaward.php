<!--IDZNI BIN MOHAMED RASHID - A18CS0075-->
<!-- MUHAMMAD SYUKRI BIN WAGIMAN - A18CS0163-->
<!-- GROUP NAME: PKPP-->
<!-- WEB PROGRAMMING SECTION - 03 -->
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: auto;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #ff8c66;
}
.button {
	background-color: #ff531a;
	border: .1px;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	margin: 4px 2px;
	cursor: pointer;
	border-radius: 10px;

}
.button span {
	cursor: pointer;
	display: inline-block;
	position: relative;
	transition: 0.5s;
}

.button span:after {
	content: '\00bb';
	position: absolute;
	opacity: 0;
	top: 0;
	right: -20px;
	transition: 0.5s;
}

.button:hover span {
	padding-right: 25px;
}

.button:hover span:after {
	opacity: 1;
	right: 0;
}
</style>
</head>
<body>
<?php
require_once('database/all.php');
if (isset($_GET['awardid'])) {
          $vendorid=$_GET['awardid'];
          $sql="SELECT * FROM `award` WHERE (`AwardID`='".$vendorid."')";
          $result=mysqli_query($virtual_con,$sql);
          $row=mysqli_fetch_assoc($result);
        }
else if (isset($_POST['awardid'])) {
          $sqlupdate="UPDATE `award` SET `Award` = '".$_POST['award']."' WHERE `AwardID` = ".$_POST['awardid'];

          $result=mysqli_query($virtual_con,$sqlupdate);
          $to="index.php";
          if ($result>0){
            //delete  Success
          $msg="Update was Success";
          }else{
            //delete failure
            $msg="Update is not successful";
          }
          goto2($to,$msg);
        }
?>
<form name="updateaward" action="updateaward.php" method="post">
  <table>
    <div class="form-group">
      <tr>
        <td><label for="Awardid">Award #</label></td>
        <td><input readonly name="awardid" type="text" maxlength="4" size="4" value="<?php echo $row['AwardID'];?>" /></td>
      </tr>
    </div>
    <div class="form-group">
      <tr>
        <td><label for="Award">Award details</label></td>
        <td><input name="award" type="text" value="<?php echo $row['Award'];?>" /></td>
      </tr>
    </div>
  </table><br>
  <div class="form-group" align="center">
    <button type="submit" class="button"><span>Update</span></button>
  </div>
</form>
</body>
