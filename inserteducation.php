<!--IDZNI BIN MOHAMED RASHID - A18CS0075-->
<!-- MUHAMMAD SYUKRI BIN WAGIMAN - A18CS0163-->
<!-- GROUP NAME: PKPP-->
<!-- WEB PROGRAMMING SECTION - 03 -->
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: auto;

}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #ff8c66;
}
.button {
	background-color: #ff531a;
	border: .1px;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	margin: 4px 2px;
	cursor: pointer;
	border-radius: 10px;

}
.button span {
	cursor: pointer;
	display: inline-block;
	position: relative;
	transition: 0.5s;
}

.button span:after {
	content: '\00bb';
	position: absolute;
	opacity: 0;
	top: 0;
	right: -20px;
	transition: 0.5s;
}

.button:hover span {
	padding-right: 25px;
}

.button:hover span:after {
	opacity: 1;
	right: 0;
}
</style>
</head>
<body>
  <?php
  require_once('database/all.php');

  if (isset($_POST['education'])){
  $insertsql="INSERT INTO `education` (`EducationID`, `Place`, `Course`, `Cgpa`) VALUES ('".$_POST['awardid']."', '".$_POST['place']."', '".$_POST['course']."', '".$_POST['cgpa']."')";
      $result=mysqli_query($virtual_con,$insertsql);
      $to="index.php";
      if ($result>0){
        //delete  Success
      $msg="Insert was Success";
      }else{
        //delete failure
        $msg="Insert is not successful";
      }
      goto2($to,$msg);
  }else {
  $sqlchkrow="select max(EducationID) as m from education";
  $result=mysqli_query($virtual_con,$sqlchkrow);
  $row=mysqli_fetch_assoc($result);
  $maxval=$row['m'];
  ?>

  <form name="inserteducation" action="inserteducation.php" method="post">
  					<table>
  						<div class="form-group">
  							<tr>
  								<td><label for="EducationID">Education #</label></td>
  								<td><input readonly name="educationid" maxlength="4" size="4" type="text" value="<?php echo $maxval+1; ?>"  /></td>
  							</tr>
  						</div>
  						<div class="form-group">
  							<tr>
  								<td><label for="Place">Place</label></td>
  								<td><input name="place" type="textarea" /></td>
  							</tr>
  						</div>
              <div class="form-group">
  							<tr>
  								<td><label for="Course">Course</label></td>
  								<td><input name="course" type="textarea" /></td>
  							</tr>
  						</div>
              <div class="form-group">
  							<tr>
  								<td><label for="Cgpa">CGPA</label></td>
  								<td><input name="cgpa" type="textarea" /></td>
  							</tr>
  						</div>
  					</table><br>
		<div class="form-group" align="center">
			<button type="submit" class="button"><span>Insert</span></button>
		</div>
</form>
<?php } ?>
</body>
