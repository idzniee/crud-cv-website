-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for resume
CREATE DATABASE IF NOT EXISTS `resume` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `resume`;

-- Dumping structure for table resume.award
CREATE TABLE IF NOT EXISTS `award` (
  `AwardID` int(11) NOT NULL DEFAULT 0,
  `Award` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AwardID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table resume.award: ~8 rows (approximately)
/*!40000 ALTER TABLE `award` DISABLE KEYS */;
INSERT INTO `award` (`AwardID`, `Award`) VALUES
	(1, 'Google Analytics Certified Developer'),
	(2, 'Mobile Web Specialist - Google Certification'),
	(3, '1st Place - University of Colorado Boulder - Emerging Tech Competition 2009'),
	(4, '1st Place - University of Colorado Boulder - Adobe Creative Jam 2008 (UI Design Category)'),
	(5, '2nd Place - University of Colorado Boulder - Emerging Tech Competition 2008'),
	(6, '1st Place - James Buchanan High School - Hackathon 2006'),
	(7, '3rd Place - James Buchanan High School - Hackathon 2005'),
	(8, 'POSTGRESQL');
/*!40000 ALTER TABLE `award` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
